package ru.romanow.generator;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.boot.web.support.SpringBootServletInitializer;

/**
 * Created by romanow on 02.09.16
 */
@SpringBootApplication
public class UUIDGeneratorApplication
        extends SpringBootServletInitializer {

    public static void main(String[] args) {
        SpringApplication.run(UUIDGeneratorApplication.class, args);
    }
}