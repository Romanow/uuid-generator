package ru.romanow.generator.web;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.validation.annotation.Validated;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;
import ru.romanow.generator.service.UUIDGeneratorService;

import javax.validation.constraints.Max;
import java.util.List;
import java.util.UUID;
import java.util.stream.Collectors;

/**
 * Created by ronin on 18.03.17
 */
@Validated
@RestController
@RequestMapping("/uuid")
public class UUIDGeneratorController {
    private static final Logger logger = LoggerFactory.getLogger(UUIDGeneratorController.class);

    @Autowired
    private UUIDGeneratorService generatorService;

    @GetMapping("/generate")
    public List<String> generate(@Max(200) @RequestParam(value = "count", required = false) Integer count) {
        return generatorService
                .generate(count != null ? count : 1)
                .stream()
                .map(UUID::toString)
                .collect(Collectors.toList());
    }
}
