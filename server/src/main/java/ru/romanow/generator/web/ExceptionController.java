package ru.romanow.generator.web;

import org.hibernate.validator.internal.engine.path.PathImpl;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.http.HttpStatus;
import org.springframework.web.bind.annotation.ExceptionHandler;
import org.springframework.web.bind.annotation.ResponseStatus;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.bind.annotation.RestControllerAdvice;
import ru.romanow.generator.web.model.ErrorResponse;

import javax.validation.ConstraintViolation;
import javax.validation.ConstraintViolationException;
import java.util.Set;
import java.util.stream.Collectors;

/**
 * Created by ronin on 18.03.17
 */
@RestControllerAdvice(annotations = RestController.class)
public class ExceptionController {
    private static final Logger logger = LoggerFactory.getLogger(ExceptionController.class);

    @ResponseStatus(HttpStatus.BAD_REQUEST)
    @ExceptionHandler(ConstraintViolationException.class)
    public ErrorResponse badRequest(ConstraintViolationException exception) {
        return new ErrorResponse(prepareErrorResponse(exception.getConstraintViolations()));
    }

    @ResponseStatus(HttpStatus.INTERNAL_SERVER_ERROR)
    @ExceptionHandler(Exception.class)
    public ErrorResponse exception(Exception exception) {
        logger.error("", exception);
        return new ErrorResponse(exception.getMessage());
    }

    private String prepareErrorResponse(Set<ConstraintViolation<?>> violations) {
        return String.format("Request validation error: [%s]",
                             violations.stream()
                                       .map(v -> String.format("Param [%s] is %s, but %s",
                                                               ((PathImpl)(v.getPropertyPath())).getLeafNode(),
                                                               v.getInvalidValue(), v.getMessage()))
                                       .collect(Collectors.joining(", ")));
    }
}