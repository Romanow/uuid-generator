package ru.romanow.generator.service;

import java.util.List;
import java.util.UUID;

/**
 * Created by ronin on 18.03.17
 */
public interface UUIDGeneratorService {
    List<UUID> generate(int count);
}
