package ru.romanow.generator.service;

import org.springframework.stereotype.Service;

import java.util.List;
import java.util.UUID;
import java.util.stream.Collectors;
import java.util.stream.IntStream;

/**
 * Created by ronin on 18.03.17
 */
@Service
public class UUIDGeneratorServiceImpl
        implements UUIDGeneratorService {

    @Override
    public List<UUID> generate(int count) {
        return IntStream.rangeClosed(1, count)
                        .mapToObj(i -> UUID.randomUUID())
                        .collect(Collectors.toList());
    }
}
