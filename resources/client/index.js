import React from 'react';
import ReactDOM from 'react-dom';

import 'bootstrap/dist/css/bootstrap.css';
import './views/global.css';

import Main from "./views/main";

ReactDOM.render(<Main/>, document.getElementById("app"));