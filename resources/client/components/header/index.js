import React from "react";
import {Button, FormGroup, InputGroup, FormControl, ControlLabel} from "react-bootstrap";

export default class Header extends React.Component {
    constructor(props) {
        super(props);
        this.state = {value: 0};

        this.handleChange = this.handleChange.bind(this);
    }

    handleChange(e) {
        this.setState({value: e.target.value});
    }

    render() {
        return (
            <form>
                <FormGroup>
                    <ControlLabel>Введите количество uuid для генерации и нажмите enter</ControlLabel>
                    <InputGroup>
                        <FormControl type="text" value={this.state.value} onChange={this.handleChange}/>
                        <InputGroup.Button>
                            <Button>Применить</Button>
                        </InputGroup.Button>
                    </InputGroup>
                </FormGroup>
            </form>
        )
    }
}